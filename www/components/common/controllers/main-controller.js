commonModule.controller('MainController', ['$scope', '$state', 'AuthenticationService', 'CommonService', '$uibModal', '$log', function($scope, $state, AuthenticationService, CommonService, $uibModal, $log) {
    console.log("In main cotroller");
    // $scope.goToProfile = function() {
    //     $state.go('app.main.profile', {
    //         userData: {
    //             name: 'pawan',
    //             email: 'pawan@gmail.com'
    //         }
    //     });
    // }

    
    
    $scope.init = function() {


        //  MenuService.getRaceSportsTypes();
    }


    $scope.items = ['item1', 'item2', 'item3'];

    $scope.animationsEnabled = true;

    $scope.open = function(size) {

        var modalInstance = $uibModal.open({
            animation: $scope.animationsEnabled,
            templateUrl: 'components/home/templates/menu.html',
            controller: 'MegaMenuController',
            size: size,
            resolve: {
                items: function() {
                    return $scope.items;
                }
            }
        });

        modalInstance.result.then(function(selectedItem) {
            $scope.selected = selectedItem;
        }, function() {
            $log.info('Modal dismissed at: ' + new Date());
        });
    };

    $scope.toggleAnimation = function() {
        $scope.animationsEnabled = !$scope.animationsEnabled;
    };


}]);
