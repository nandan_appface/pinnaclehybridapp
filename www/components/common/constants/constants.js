commonModule
    .constant("ErrorCode", {
        "SUCCESS": 0,
        "FAILURE": 1,
        "UNAUTHENTICATED": 2,
        "UNAUTHORIZED": 401,
        "BAD_PARAMETER": 400,
        "REQUEST_TIMEOUT": 408
    })
    .constant("ServiceUrl", {
        "BASE_URL": {
            "prod": "https://devapp.bettortest.com/mobile1",
            "dev": "https://devapp.bettortest.com/mobile",
        },

        //Account Module APIs
        "SIGN_IN": "/account/signIn",
        "SIGN_OUT": "/account/signOut",
        "REGISTER": "/account/signUp",
        "IF_EXISTS": "/account/exists",

        "FORGET_PASSWORD": "/account/password_forget_email",
        "RESET_PASSWORD": "/account/password_reset",
        "GET_USER_PROFILE": "/account/current_account",
        "UPDATE_USER_PROFILE": "/account/profile_update",
        "WALLET_STATUS" : "/account/wallet",

        //Common APIs
        "RACE_SPORT_TYPES": "/common/raceSportTypes",
        "HOME_MENU": "/common/homeMenu",
        "GET_HORSE_RACING_MENU": "/common/racingMenu/R",
        "GET_TROT_RACING_MENU": "/common/racingMenu/T",
        "GET_GREYHOUND_RACING_MENU": "/common/racingMenu/G",
        "COUNTRY_LIST": "/common/countryList",

        //Racing Data APIs
        "GET_HORSE_TODAYS_RACING_DATA": "/racingdata/raceList/TODAY/R",
        "GET_TROT_TODAYS_RACING_DATA": "/racingdata/raceList/TODAY/T",
        "GET_GREYHOUND_TODAYS_RACING_DATA": "/racingdata/raceList/TODAY/G",

        "GET_HORSE_NEXTOGO_RACING_DATA": "/racingdata/nextToGoRaces/R",
        "GET_TROT_NEXTOGO_RACING_DATA": "/racingdata/nextToGoRaces/T",
        "GET_GREYHOUND_NEXTOGO_RACING_DATA": "/racingdata/nextToGoRaces/G",

        "GET_HORSE_TOMORROWS_RACING_DATA": "/racingdata/raceList/TOMORROW/R",
        "GET_TROT_TOMORROWS_RACING_DATA": "/racingdata/raceList/TOMORROW/T",
        "GET_GREYHOUND_TOMORROWS_RACING_DATA": "/racingdata/raceList/TOMORROW/G",

        "GET_HORSE_RACE_RESULTS": "/racingdata/results/TODAY/R",
        "GET_TROT_RACE_RESULTS": "/racingdata/results/TODAY/T",
        "GET_GREYHOUND_RACE_RESULTS": "/racingdata/results/TODAY/G",

        "GET_RACE_DETAILS": "/racingdata/race",
        "GET_RACE_POOLS": "/racingdata/pools",

        "GET_MULTI_LEGS_RACING_DATA": "/racingdata/multilegs",

        "GET_HORSE_FUTURES_RACING_DATA": "/racingdata/raceList/FUTURE/R",
        "GET_TROT_FUTURES_RACING_DATA": "/racingdata/raceList/FUTURE/T",
        "GET_GREYHOUND_FUTURES_RACING_DATA": "/racingdata/raceList/FUTURE/G",

        "GET_HORSE_EARLY_MARKET_RACING_DATA": "/racingdata/raceList/EARLYMARKET/R",
        "GET_TROT_EARLY_MARKET_RACING_DATA": "/racingdata/raceList/EARLYMARKET/T",
        "GET_GREYHOUND_EARLY_MARKET_RACING_DATA": "/racingdata/raceList/EARLYMARKET/G",

        "GET_HOME_MENU": "/common/homeMenu",
        "NEXT_TO_JUMP_RACES":"/racingdata/nextToGoRaces/ALL",
        "ADD_OR_EDIT_BETS":"/racingdata/pools/entrant/",
        "BET_SUBMISSION_FOR_SINGLES":"/wagers/race",
        "WAGER_STATUS" : "/wagers/race/"



    });
