racingModule.controller('TomorrowsRaceController', ['$scope', '$stateParams', 'RacingService', function($scope, $stateParams, RacingService) {

    // $scope.rows = [
    // 	"BAIRNSDALE",
    // 	"COFFS HARBOUR",
    // 	"PORTLAND MEADOWS",
    // 	"QUEANBEYAN",
    // 	"SANTIAGO",
    // ]

    $scope.init = function() {



        $scope.racingData = "";


        switch ($stateParams.source) {

            case "R":
            	$scope.pageTitle = "HORSE RACING";
            	$scope.icon = "images/orangeicons/horse.png";
                RacingService.getHorseTomorrowsRacingData().then(function(res) {
                    if (res.result) {

                        $scope.racingData = res.data;

                    }

                });
                break;
            case "T":
            $scope.pageTitle = "HARNESS RACING";
            	$scope.icon = "images/orangeicons/trot.png";
                RacingService.getTrotTomorrowsRacingData().then(function(res) {
                    if (res.result) {

                        $scope.racingData = res.data;

                    }

                });
                break;

            case "G":
            	$scope.pageTitle = "GREYHOUND RACING";
            	$scope.icon = "images/orangeicons/greyhound.png";
                RacingService.getGreyhoundTomorrowsRacingData().then(function(res) {
                    if (res.result) {

                        $scope.racingData = res.data;

                    }

                });
                break;



        }



    

}
    

}]);
