racingModule.controller('RaceDetailPageController', ['$scope', '$stateParams', 'RacingService', '$filter', '$timeout', 'RouteHistoryService', function($scope, $stateParams, RacingService, $filter, $timeout, RouteHistoryService) {



    $scope.init = function() {

        $scope.isExoticsMenuOpen = false;


        RacingService.getRaceDetails($stateParams.id).then(function(res) {
            if (res.result) {
                $scope.raceDetails = res.data;
                $scope.list = $scope.raceDetails.peers
                $scope.setDefaultActiveSilderItem($stateParams.id);


                //Singles Dropdowns

                $scope.singlesDropdowns = {};

               
                $scope.singlesDropdowns.leftSinglesMenuOptions = Object.keys($scope.raceDetails.singlesMenu);
                $scope.singlesDropdowns.leftSelectedSinglesMenuOption = $scope.singlesDropdowns.leftSinglesMenuOptions[0];
                $scope.singlesDropdowns.rightSinglesMenuOptions = $scope.raceDetails.singlesMenu[$scope.singlesDropdowns.leftSelectedSinglesMenuOption];
                $scope.singlesDropdowns.rightSelectedSinglesMenuOption = $scope.singlesDropdowns.rightSinglesMenuOptions[0];


                //Exotics Dropdowns
                $scope.exoticsDropdowns = {};

                $scope.exoticsDropdowns.exoticsMenuOptions = $scope.raceDetails.exoticsMenu;
               



                //Tabs Control
                if ($scope.raceDetails.exoticsMenu == undefined || $scope.raceDetails.exoticsMenu.length == 0) {
                    $scope.exoticsTabDisabled = true;
                } else {
                    $scope.exoticsTabDisabled = false;
                }
            } else {
                alert(res.message);
                RouteHistoryService.goBack();
            }
            // $timeout($scope.init, 1000);
        });





    }



    $scope.onSingesLeftDropdownChange = function() {
        $scope.singlesDropdowns.rightSinglesMenuOptions = $scope.raceDetails.singlesMenu[$scope.singlesDropdowns.leftSelectedSinglesMenuOption];
        $scope.singlesDropdowns.rightSelectedSinglesMenuOption = $scope.singlesDropdowns.rightSinglesMenuOptions[0];
    }


    $scope.onSinglesRightDropdownChange = function() {
        var keys = $scope.raceDetails.dropDownRequest[$scope.singlesDropdowns.rightSelectedSinglesMenuOption];
        RacingService.getRacePools(keys.join(',')).then(function(res) {
            if (res.result) {
                $scope.raceDetails.pools = res.data.pools;
                $scope.raceDetails.odds = res.data.odds;

            }

        });
    }

    $scope.onExoticsMenuClick = function(menuName, event) {

        if ($scope.isExoticsMenuOpen) {
            return;
        }
        switch (menuName) {
            case 'Quinella':
            case 'Exacta':
            case 'Duet':
                $scope.exoticsCheckBoxCount = ['one', 'two'];
                break;
            case 'Trifecta':
                $scope.exoticsCheckBoxCount = ['one', 'two', 'three'];
                break;
            case 'First Four':
                $scope.exoticsCheckBoxCount = ['one', 'two', 'three', 'four'];
                break;

        }
        $scope.checkBoxCount
        var keys = $scope.raceDetails.dropDownRequest[menuName];


        RacingService.getRacePools(keys.join(',')).then(function(res) {
            if (res.result) {
                $scope.raceDetails.pools = res.data.pools;
                $scope.raceDetails.odds = res.data.odds;

            }

        });

    }


    $scope.raceSelect = "Quinella";
    $scope.setActive = function(item, list) {
        list.some(function(item) {
            if (item.active) {
                return item.active = false;
            }
        });
        item.active = true;
    };


    $scope.setDefaultActiveSilderItem = function(id) {
        var obj = $filter('filter')($scope.list, {
            value: id
        })[0];
        obj.active = true;
    }

    $scope.onSliderItemClick = function(item, list) {
        RacingService.getRaceDetails(item.value).then(function(res) {
            if (res.result) {
                $scope.raceDetails = res.data;
                $scope.setActive(item, list);

            }

        });
    };

    $scope.exoticsMenuGroupclick = function(event) {
        $scope.isExoticsMenuOpen = angular.element(event.currentTarget).hasClass('panel-open');
    };

    $scope.addSingleBet = function(entrant, raceDetails, pool){

        if(raceDetails.race.status == "OPEN"){
                var raceSubType = $scope.singlesDropdowns.rightSelectedSinglesMenuOption
                RacingService.addBetSlipToLocalStorage(entrant, raceDetails, pool, raceSubType);
            
        }
        
    };

   var ExoticData =[], col1 = [], col2=[], combos=[];
    $scope.addExoticBets = function(entrant, raceDetails, index, rowindex, chkModel, exoticname){
        if(raceDetails.race.status == "OPEN"){
            if(chkModel){
                 var bets = {venue:"", entrantId:"", raceNumber:"", value:0.0, c1: [], c2: [], exoticname:""};
                bets.exoticname = exoticname;
                bets.venue = raceDetails.race.venue;
                bets.raceNumber = raceDetails.race.raceNumber;
                bets.entrantId = entrant.id;
                if (index == 0) {
                    col1.push(rowindex+1);
                } else{
                    col2.push(rowindex+1);
                };
                
                bets.c1 = col1;
                bets.c2 = col2;
                
                ExoticData.unshift(bets);
                combos = RacingService.findCombinations(col1, col2);
                console.log(ExoticData.length);
                $scope.ExoticsCombos = combos.length;

            }else{
                console.log(1);
                if(ExoticData){
                    for (var i = 0; i < ExoticData.length; i++) {
                        if(ExoticData[i].entrantId == entrant.id){
                            ExoticData.shift(ExoticData[i])
                        }
                    };
                }
                    
            }
        }
    };

    $scope.addExoticBetsToBetSlip = function(){
        RacingService.addExoticBetsToBetSlip(ExoticData[0], $scope.ExoticsCombos);
    }

}]);

    // var combos = [] //or combos = new Array(2);
    // var array1=["1","2","3"];

    // var array2=["5","6","7"];
    // for(var i = 0; i < array1.length; i++)
    // {
    //      for(var j = 0; j < array2.length; j++)
    //      {
    //         combos.push(array1[i] + array2[j]);
    //      }
         
    // }