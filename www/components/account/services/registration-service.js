accountModule.factory('RegistrationService', ['ServiceUrl', 'HttpRequester', 'ErrorCode', '$q', function(ServiceUrl, HttpRequester, ErrorCode, $q) {
    var registrationService = {};

    registrationService.register = function(userInfo) {

        //Preparing data
        var data = {
            "username": userInfo.userName ? userInfo.userName : "",
            "password": userInfo.password ? userInfo.password : "",
            "firstName": userInfo.firstName ? userInfo.firstName : "",
            "lastName": userInfo.lastName ? userInfo.lastName : "",
            "dob": userInfo.dob ? userInfo.dob : "", //"28/03/1980"
            "emailAddress": userInfo.email ? userInfo.email : "",
            "nickname": userInfo.nickname ? userInfo.nickname : "",
            "mobileNumber": userInfo.mobileNumber ? userInfo.mobileNumber : "",
            "countryCode": userInfo.country.dialingCode ? userInfo.country.dialingCode : "", //"+61"
            "areaCode": userInfo.areaCode ? userInfo.areaCode : "",
            "phoneNumber": userInfo.phoneNumber ? userInfo.phoneNumber : "",
            "addressFlatNumber": userInfo.addressFlatNumber ? userInfo.addressFlatNumber : "",
            "addressStreetNumber": userInfo.addressStreetNumber ? userInfo.addressStreetNumber : "",
            "addressStreet": userInfo.addressStreet ? userInfo.addressStreet : "",
            "addressStreetType": userInfo.addressStreetType ? userInfo.addressStreetType : "", //"Street"
            "addressSuburb": userInfo.addressSuburb ? userInfo.addressSuburb : "",
            "addressPostcode": userInfo.addressPostcode ? userInfo.addressPostcode : "",
            "addressState": userInfo.addressState ? userInfo.addressState : "",
            "addressCountry": userInfo.country.name ? userInfo.country.name : "",
            "pin": userInfo.pin ? userInfo.pin : "",
            "affiliateCode": userInfo.affiliateCode ? userInfo.affiliateCode : "" //"This value can be null."
        };


        var url = ServiceUrl.REGISTER;
        var params = "";
        var method = "POST";
        var deferred = $q.defer();

        //Making Http Request
        HttpRequester.makeRequest(method, url, params, data).then(function(res) {

            if (res.errorCode == ErrorCode.SUCCESS) {
                deferred.resolve({
                    result: true,
                    data: res
                });
            } else {
                deferred.resolve({
                    result: false,
                    message: res.errorMessage
                });
            }

        });
        return deferred.promise;
    }



    registrationService.ifExists = function(key, value) {

        //Preparing data
        var data = ""
        var params = {
            "type" : key,
            "value" : value 
        };
        


        var url = ServiceUrl.IF_EXISTS;

        var method = "GET";
        var deferred = $q.defer();

        //Making Http Request
        HttpRequester.makeRequest(method, url, params, data).then(function(res) {

            if (res.errorCode == ErrorCode.SUCCESS) {
                deferred.resolve({
                    result: true,
                    exists: res.data.exists
                });
            } else {
                deferred.resolve({
                    result: false,
                    exists: res.errorMessage
                });
            }

        });
        return deferred.promise;
    }




    return registrationService;
}]);
