racingModule.controller('BetSlipConfirmController', ['$scope', '$stateParams','RouteHistoryService','RacingService','$state', function($scope, $stateParams, RouteHistoryService, RacingService, $state) {

	 var bets= JSON.parse(localStorage.getItem("betSlipData"));
     var betsData = [];
    for(var i=0;i<bets.length;i++){
        var total = 0;
        var odds = 0;
        if(bets[i].value > 0){
            var referenceId = Math.floor(Math.random()*90000) + 10000;
            bets[i].referenceId = referenceId;
            betsData.push(bets[i]);
        }
    }
    
    $scope.betSlipConfirmData = betsData;

    var betConfirmData = { 
	    fixedOddsWagerRequests : [],
	    toteWagerRequests : []
	};


	$scope.totalBet = function(){
            var total = 0;
            var odds = 0;
            for(count=0;count<$scope.betSlipConfirmData.length;count++){
                total +=$scope.betSlipConfirmData[count].value;
                odds += parseFloat($scope.betSlipConfirmData[count].odds * $scope.betSlipConfirmData[count].value);
                var referenceId = Math.floor(Math.random()*90000) + 10000;
                $scope.betSlipConfirmData[count].referenceId = referenceId;
                
            }
            $scope.betTotalConfirmValue = total;
            $scope.betEstimateTotalConfirmValue = odds;
            return true;
    };

	$scope.goBack = function() {

        RouteHistoryService.goBack();
    };

    $scope.confirmBets = function(){
    	var bets = $scope.betSlipConfirmData;
    	for(var i=0;i<bets.length;i++){
    		betConfirmData.fixedOddsWagerRequests.push({ 
    		        "referenceId" : bets[i].referenceId,
    		        "oddsId" : bets[i].oddsId,
    		        "amount" : bets[i].value * bets[i].odds * 100,
    		        "fixedOdds" : bets[i].odds * 100
    		    });

    	}
    	
    	RacingService.betConfirm(betConfirmData).then(function(res) {
            if (res.result) {
                localStorage.setItem("betSlipData",JSON.stringify($scope.betSlipConfirmData));
                $scope.betResult = res.data.data.responses;
                $state.go('app.betsuccess', {wagers: $scope.betResult});
                
            }else{
            	alert("Data Not Found.");
            }

        });
    };
    
}]);