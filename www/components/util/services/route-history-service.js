utilModule.factory('RouteHistoryService', ['$rootScope', '$state', function($rootScope, $state) {
    var routeHistoryService = {};

    var routeHistory = [];


    $rootScope.$on('$stateChangeSuccess', function(ev, to, toParams, from, fromParams) {

        if (!$rootScope.calledGoBack) {
            routeHistory.push({
                route: from,
                routeParams: fromParams
            });
        }
        $rootScope.calledGoBack = false;

    });

    routeHistoryService.getRouteHistory = function() {
        return routeHistory;
    }


    routeHistoryService.goBack = function() {
        $rootScope.calledGoBack = true;
        var state = routeHistory.pop();
        if (state && state.route.name!='') {

            $state.go(state.route.name, state.routeParams);
        }


    }

    return routeHistoryService;
}]);
