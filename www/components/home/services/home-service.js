homeModule.factory('HomeService', ['ServiceUrl', 'HttpRequester', 'ErrorCode', '$q', '$filter', function(ServiceUrl, HttpRequester, ErrorCode, $q, $filter) {
    var homeService = {};
    homeService.urlMap = {
        "Racing": "app.main.races",
        "SignUp": "app.main.login"
    };

    homeService.getHomeImages = function() {
        var data = ""
        var url = ServiceUrl.GET_HOME_MENU;
        var params = "";
        var method = "GET";
        var deferred = $q.defer();
        HttpRequester.makeRequest(method, url, params, data).then(function(res) {

            if (res.errorCode == ErrorCode.SUCCESS) {


                angular.forEach(res.data.entries, function(entry) {
                    entry.state = homeService.urlMap[entry.key];

                });

                deferred.resolve({
                    result: true,
                    data: res.data.entries

                });
            } else {
                deferred.resolve({
                    result: false,
                    message: res.errorMessage
                });
            }

        });
        return deferred.promise;
    }

    homeService.nextToJumpRaces = function() {
        var data = ""
        var url = ServiceUrl.NEXT_TO_JUMP_RACES;
        var params = "";
        var method = "GET";
        var deferred = $q.defer();
        HttpRequester.makeRequest(method, url, params, data).then(function(res) {

            if (res.errorCode == ErrorCode.SUCCESS) {
                deferred.resolve({
                    result: true,
                    data: res.data

                });
            } else {
                deferred.resolve({
                    result: false,
                    message: res.errorMessage
                });
            }

        });
        return deferred.promise;
    }

    return homeService;
}]);
