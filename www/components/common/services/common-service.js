commonModule.factory('CommonService', ['ServiceUrl', 'HttpRequester', 'ErrorCode', '$q', function(ServiceUrl, HttpRequester, ErrorCode, $q) {
    var commonService = {};
    commonService.getRaceSportsTypes = function() {
        //Preparing data
        var data = ""
        var url = ServiceUrl.RACE_SPORT_TYPES;
        var params = "";
        var method = "GET";
        var deferred = $q.defer();

        //Making Http Request
        HttpRequester.makeRequest(method, url, params, data).then(function(res) {

            if (res.errorCode == ErrorCode.SUCCESS) {
             
                deferred.resolve({
                    result: true,
                    data: res
                });
            } else {
                deferred.resolve({
                    result: false,
                    message: res.errorMessage
                });
            }

        });
        return deferred.promise;
    }

        commonService.getCountryList = function() {

        //Preparing data
        var data = ""
        var url = ServiceUrl.COUNTRY_LIST;
        var params = "";
        var method = "GET";
        var deferred = $q.defer();

        //Making Http Request
        HttpRequester.makeRequest(method, url, params, data).then(function(res) {

            if (res.errorCode == ErrorCode.SUCCESS) {
                deferred.resolve(res.data);
            }

        });
        return deferred.promise;
    }

    return commonService;
}]);
