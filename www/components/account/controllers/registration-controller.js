accountModule.controller('RegistrationController', ['$scope', '$state', 'RegistrationService', 'RouteHistoryService', '$filter', 'CommonService', 'AuthenticationService', function($scope, $state, RegistrationService, RouteHistoryService, $filter, CommonService, AuthenticationService) {



    $scope.goBack = function() {
        RouteHistoryService.goBack();
    }

    $scope.stepsCompleted = {
        step1 : false,
        step2 : false,
        step3 : false,
        step4 : false,
        step5 : false,
    };

    var currentDate = new Date();
    var n = currentDate.getFullYear();
    var validYear = n - 18;
    currentDate.setFullYear(validYear);
    $scope.maxDate = currentDate;

    $scope.userInfo = {};
    $scope.validateInfo = {};
    $scope.validForm = false;
    $scope.init = function() {
        CommonService.getCountryList().then(function(result) {
            $scope.countries = result.countryList;
       
              var obj = $filter('filter')($scope.countries, {code:"AU"});
              $scope.userInfo.country =  obj[0];
        })

    }


    $scope.goToStep2 = function() {
        if ($scope.userInfo.country) {
            $scope.stepsCompleted.step1 = true;
            $state.go('app.registration.step2');
        } else {

            alert("Please select the country");
        }

    }

    $scope.goToStep3 = function() {
        var msg = "";
        var validate = true;

        if (!$scope.userInfo.firstName) {
            msg += "Please enter First Name";
            validate = false;
        }
        if (!$scope.userInfo.lastName) {
            msg += "\nPlease enter Surname Name";
            validate = false;

        }



        if (!$scope.userInfo.dob) {
            msg += "\nPlease enter Date of Birth";
            validate = false;
        } else {
            if ($scope.userInfo.dob > $scope.maxDate) {
                msg = "Please Enter a valid date. (You should be 18+)";
                validate = false;
            } else {
                $scope.userInfo.dob = $filter('date')($scope.userInfo.dob, "dd/MM/yyyy");
            }

        }


        if (validate) {
            $scope.stepsCompleted.step2 = true;
            $state.go('app.registration.step3');
        } else {
            alert(msg);
        }



    }

    $scope.goToStep4 = function() {
        var msg = "";
        var validate = true;

        if (!$scope.userInfo.addressStreetNumber) {
            msg += "Please enter Street Number";
            validate = false;
        }
        if (!$scope.userInfo.addressStreet) {
            msg += "Please enter Street Name";
            validate = false;
        }
        if (!$scope.userInfo.addressSuburb) {
            msg += "\nPlease enter Suburb";
            validate = false;

        }

        if (!$scope.userInfo.addressState) {
            msg += "\nPlease enter State";
            validate = false;
        }

        if (!$scope.userInfo.addressPostcode) {
            msg += "\nPlease enter Post Code";
            validate = false;
        }
        else {
             if ($scope.userInfo.addressPostcode.length < 4) {
                msg += "Post code should be atleast 4 characters long";
                validate = false;
            }

        }



        if (validate) {
            $scope.stepsCompleted.step3 = true;
            $state.go('app.registration.step4');
        } else {
            alert(msg);
        }


    }

    $scope.goToStep5 = function() {

        var msg = "";
        var validate = true;

        if (!$scope.userInfo.email) {
            msg += "Please enter Email";
            validate = false;
        }
        if (!$scope.validateInfo.confirmEmail) {
            msg += "\nPlease enter Confirm Email";

            validate = false;

        }

        if (!$scope.userInfo.mobileNumber) {
            msg += "\nPlease enter Mobile Number";

            validate = false;

        }

        if ($scope.userInfo.email && $scope.validateInfo.confirmEmail) {
            if ($scope.userInfo.email != $scope.validateInfo.confirmEmail) {
                msg = "Email and Confirm Email did not match";

                validate = false
            }

        }


        if (validate) {
            $scope.stepsCompleted.step4 = true;
            $state.go('app.registration.step5');
        } else {
            alert(msg);
        }


    }

    $scope.register = function() {

        var msg = "";
        var validate = true;

        if (!$scope.userInfo.userName) {
            msg += "Please enter Username";
            validate = false;
        }


        if (!$scope.userInfo.password) {
            msg += "\nPlease enter Password";

            validate = false;

        } else {
            if ($scope.userInfo.password.length < 6) {
                msg += "Password should be atleast 6 characters long";
                validate = false;
            }
        }


        if (!$scope.validateInfo.confirmPassword) {
            msg += "\nPlease enter Confirm Password";

            validate = false;

        }

        if (!$scope.userInfo.pin) {
            msg += "\nPlease enter Security Number";

            validate = false;

        } else {
            if ($scope.userInfo.pin.length < 4) {
                alert("Minimum length should be 4");
                validate = false;

            }

        }

        if (!$scope.validateInfo.confirmPin) {
            msg += "\nPlease enter Confirm Security Number";

            validate = false;

        }

        if ($scope.userInfo.password && $scope.validateInfo.confirmPassword) {

            if ($scope.userInfo.password != $scope.validateInfo.confirmPassword) {
                msg = "Password and Confirm Password did not match";

                validate = false
            }

        }

        if ($scope.userInfo.pin && $scope.validateInfo.confirmPin) {

            if ($scope.userInfo.pin != $scope.validateInfo.confirmPin) {
                msg = "Security Number and Confirm Security Number did not match";

                validate = false
            }

        }

        if (!$scope.validateInfo.declarationOne || !$scope.validateInfo.declarationTwo) {
            msg = "Please accept the terms & conditions";
            validate = false
        }

        if (validate) {

            if($scope.stepsCompleted.step1  && $scope.stepsCompleted.step2 && $scope.stepsCompleted.step3 && $scope.stepsCompleted.step4){
                            RegistrationService.register($scope.userInfo).then(function(result) {
                if (result.result) {
                    alert("Your account is successfully created.");

                    AuthenticationService.login($scope.userInfo.userName, $scope.userInfo.password).then(function(result) {
                        if (result.result) {


                            $state.go('app.main.home');


                        } else {
                            var msg = ""
                            if (result.message) {
                                msg = result.message;
                            } else {
                                msg = "Login failed";
                            }

                            alert(msg);

                        }
                    });




                } else {

                    alert(result.message);
                }
            });
            }
            else {
                alert("Please complete previous registration steps")
            }

        } else {

            alert(msg);
        }

    }

    $scope.checkUsername = function() {
       
        if (!$scope.userInfo.userName) {
            alert("Please enter Username");
           
        } else {
            RegistrationService.ifExists("USERNAME", $scope.userInfo.userName).then(function(res){
                if(res.result && res.exists){
                   
                    alert("Username already exists");
                }
                
            })
        }

       
    }


        $scope.checkEmail = function() {
       
        if (!$scope.userInfo.email) {
            alert("Please enter Email");
           
        } else {
            RegistrationService.ifExists("EMAIL", $scope.userInfo.email).then(function(res){
                if(res.result && res.exists){
                   
                    alert("Email already exists");
                }
                
            })
        }

       
    }


        $scope.checkMobile = function() {
       
        if (!$scope.userInfo.mobileNumber) {
            alert("Please enter Mobile Number");
           
        } else {
            RegistrationService.ifExists("MOBILE", $scope.userInfo.mobileNumber).then(function(res){
                if(res.result && res.exists){
                   
                    alert("Mobile Number already exists");
                }
                
            })
        }

       
    }


}]);
