httpRequesterModule.provider('HttpRequester', function() {
    // In the provider function, you cannot inject any
    // service or factory. This can only be done at the
    // "$get" method.

    this.mode = 'dev';

    this.$get = function(ServiceUrl, $q, $http, ErrorCode, $log, usSpinnerService) {

        var httpRequester = {};
        //Configurable properties
        var mode = this.mode;
        var baseUrl = '';
        if (mode == 'prod') {
            baseUrl = ServiceUrl.BASE_URL['prod']
        } else {

            baseUrl = ServiceUrl.BASE_URL['dev']
        }

        //makeRequest() starts
        httpRequester.makeRequest = function(method, apiUrl, params, data, noSpinner) {
                var deferred = $q.defer();
                var response = '';



                var request = $http({
                    method: method,
                    url: baseUrl + apiUrl,
                    params: params,
                    data: data

                });
                if(!noSpinner){
                    
                     usSpinnerService.spin('spinner-1');
                }
               
                request.then(function(res) {

                    usSpinnerService.stop('spinner-1');
                    console.log("HttpRequester Success: " + JSON.stringify(res));

                    if (res.data.success) {
                        response = {
                            errorCode: ErrorCode.SUCCESS,
                            data: res.data
                        };
                        deferred.resolve(response);

                    } 
                    else if (res.data.errorCode == 1){
                         response = {
                            errorCode: ErrorCode.UNAUTHENTICATED,
                            errorMessage: res.data.errorMessage
                        };
                        deferred.resolve(response);
                    }

                    else {
                        response = {
                            errorCode: ErrorCode.FAILURE,
                            errorMessage: res.data.errorMessage
                        };
                        deferred.resolve(response);
                    }


                }, function(error) {
                    usSpinnerService.stop('spinner-1');
                    console.log("HttpRequester Fail: " + JSON.stringify(error));
                    response = {
                        errorCode: ErrorCode.FAILURE,
                        errorMessage: "Unknown Error"
                    };
                    deferred.resolve(response);

                });
                return deferred.promise;
            }
            //makeRequest()  ends
        return httpRequester;

    };


    this.setMode = function(mode) {
        this.mode = mode;
    };
});


