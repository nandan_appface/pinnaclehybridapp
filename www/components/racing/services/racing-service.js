racingModule.factory('RacingService', ['ServiceUrl', 'HttpRequester', 'ErrorCode', '$q', '$filter','$rootScope', function(ServiceUrl, HttpRequester, ErrorCode, $q, $filter, $rootScope) {
    var racingService = {};

    racingService.urlMap = {
        "Results": "app.main.races.result",
        "Next To Jump": "app.main.races.nextogo",
        "Today's Racing": "app.main.races.today",
        "Tomorrow's Racing": "app.main.races.tomorrow",
        "Early Markets": "app.main.races.early-market",
        "Futures": "app.main.races.futures",
        "Jockey Challenge": "",
        "Specials": "",
        "Multi-Leg": "app.main.races.multi-legs",
        "Watch! Live Racing": ""
    };
    //Racing home page  services
    racingService.getHorseRacingMenu = function() {
        var data = ""
        var url = ServiceUrl.GET_HORSE_RACING_MENU;
        var params = "";
        var method = "GET";
        var deferred = $q.defer();
        HttpRequester.makeRequest(method, url, params, data).then(function(res) {

            if (res.errorCode == ErrorCode.SUCCESS) {


                angular.forEach(res.data.entries, function(entry) {
                    entry.state = racingService.urlMap[entry.key];

                });

                deferred.resolve({
                    result: true,
                    data: res.data.entries

                });
            } else {
                deferred.resolve({
                    result: false,
                    message: res.errorMessage
                });
            }

        });
        return deferred.promise;
    }






    racingService.getTrotRacingMenu = function() {
        var data = ""
        var url = ServiceUrl.GET_TROT_RACING_MENU;
        var params = "";
        var method = "GET";
        var deferred = $q.defer();
        HttpRequester.makeRequest(method, url, params, data).then(function(res) {

            if (res.errorCode == ErrorCode.SUCCESS) {

                angular.forEach(res.data.entries, function(entry) {
                    entry.state = racingService.urlMap[entry.key];

                });
                deferred.resolve({
                    result: true,
                    data: res.data.entries
                });
            } else {
                deferred.resolve({
                    result: false,
                    message: res.errorMessage
                });
            }

        });
        return deferred.promise;
    }



    racingService.getGreyhoundRacingMenu = function() {
            var data = ""
            var url = ServiceUrl.GET_GREYHOUND_RACING_MENU;
            var params = "";
            var method = "GET";
            var deferred = $q.defer();
            HttpRequester.makeRequest(method, url, params, data).then(function(res) {


                if (res.errorCode == ErrorCode.SUCCESS) {
                    angular.forEach(res.data.entries, function(entry) {
                        entry.state = racingService.urlMap[entry.key];

                    });
                    deferred.resolve({
                        result: true,
                        data: res.data.entries
                    });
                } else {
                    deferred.resolve({
                        result: false,
                        message: res.errorMessage
                    });
                }

            });
            return deferred.promise;
        }
        //Today's racing services
    racingService.getHorseTodaysRacingData = function() {
        var data = ""
        var url = ServiceUrl.GET_HORSE_TODAYS_RACING_DATA;
        var params = "";
        var method = "GET";
        var deferred = $q.defer();
        HttpRequester.makeRequest(method, url, params, data).then(function(res) {

            var racingData = [];
            if (res.errorCode == ErrorCode.SUCCESS) {
                angular.forEach(res.data.meetings, function(data) {
                    var obj = data;
                    obj.races = res.data.races[obj.id] ? res.data.races[obj.id] : [];
                    obj.multiPools = res.data.multiPools[obj.id] ? res.data.multiPools[obj.id] : [];

                    racingData.push(data);
                  

                });

                deferred.resolve({
                    result: true,
                    data: racingData
                });
            } else {
                deferred.resolve({
                    result: false,
                    message: res.errorMessage
                });
            }

        });
        return deferred.promise;
    }

    racingService.getTrotTodaysRacingData = function() {

        var data = ""
        var url = ServiceUrl.GET_TROT_TODAYS_RACING_DATA;
        var params = "";
        var method = "GET";
        var deferred = $q.defer();
        HttpRequester.makeRequest(method, url, params, data).then(function(res) {

            var racingData = [];
            if (res.errorCode == ErrorCode.SUCCESS) {
                angular.forEach(res.data.meetings, function(data) {
                    var obj = data;
                    obj.races = res.data.races[obj.id] ? res.data.races[obj.id] : [];
                    obj.multiPools = res.data.multiPools[obj.id] ? res.data.multiPools[obj.id] : [];

                    racingData.push(obj);
                    // data.state = racingService.urlMap[entry.key];

                });

                deferred.resolve({
                    result: true,
                    data: racingData
                });
            } else {
                deferred.resolve({
                    result: false,
                    message: res.errorMessage
                });
            }

        });
        return deferred.promise;

    }

    racingService.getGreyhoundTodaysRacingData = function() {

        var data = ""
        var url = ServiceUrl.GET_GREYHOUND_TODAYS_RACING_DATA;
        var params = "";
        var method = "GET";
        var deferred = $q.defer();
        HttpRequester.makeRequest(method, url, params, data).then(function(res) {

            var racingData = [];
            if (res.errorCode == ErrorCode.SUCCESS) {
                angular.forEach(res.data.meetings, function(data) {
                    var obj = data;
                    obj.races = res.data.races[obj.id] ? res.data.races[obj.id] : [];
                    obj.multiPools = res.data.multiPools[obj.id] ? res.data.multiPools[obj.id] : [];

                    racingData.push(obj);
                });

                deferred.resolve({
                    result: true,
                    data: racingData
                });
            } else {
                deferred.resolve({
                    result: false,
                    message: res.errorMessage
                });
            }

        });
        return deferred.promise;

    }



    //Nextogo  services
    racingService.getHorseNextogoRacingData = function() {
        var data = ""
        var url = ServiceUrl.GET_HORSE_NEXTOGO_RACING_DATA;
        var params = "";
        var method = "GET";
        var deferred = $q.defer();
        HttpRequester.makeRequest(method, url, params, data).then(function(res) {
            var racingData = [];
            if (res.errorCode == ErrorCode.SUCCESS) {

                angular.forEach(res.data.races, function(data) {
                    var obj = data;
                    obj.meeting = res.data.meetings[obj.meetingId];


                    racingData.push(obj);
                    // data.state = racingService.urlMap[entry.key];

                });


                deferred.resolve({
                    result: true,
                    data: racingData
                });
            } else {
                deferred.resolve({
                    result: false,
                    message: res.errorMessage
                });
            }

        });
        return deferred.promise;
    }
    racingService.getTrotNextogoRacingData = function() {
        var data = ""
        var url = ServiceUrl.GET_TROT_NEXTOGO_RACING_DATA;
        var params = "";
        var method = "GET";
        var deferred = $q.defer();
        HttpRequester.makeRequest(method, url, params, data).then(function(res) {
            var racingData = [];
            if (res.errorCode == ErrorCode.SUCCESS) {

                angular.forEach(res.data.races, function(data) {
                    var obj = data;
                    obj.meeting = res.data.meetings[obj.meetingId];


                    racingData.push(obj);
                 

                });


                deferred.resolve({
                    result: true,
                    data: racingData
                });
            } else {
                deferred.resolve({
                    result: false,
                    message: res.errorMessage
                });
            }

        });
        return deferred.promise;
    }

    racingService.getGreyhoundNextogoRacingData = function() {
        var data = ""
        var url = ServiceUrl.GET_GREYHOUND_NEXTOGO_RACING_DATA;
        var params = "";
        var method = "GET";
        var deferred = $q.defer();
        HttpRequester.makeRequest(method, url, params, data).then(function(res) {
            var racingData = [];
            if (res.errorCode == ErrorCode.SUCCESS) {

                angular.forEach(res.data.races, function(data) {
                    var obj = data;
                    obj.meeting = res.data.meetings[obj.meetingId];


                    racingData.push(obj);
                   

                });


                deferred.resolve({
                    result: true,
                    data: racingData
                });
            } else {
                deferred.resolve({
                    result: false,
                    message: res.errorMessage
                });
            }

        });
        return deferred.promise;
    }



    //Tomorrows's racing services
    racingService.getHorseTomorrowsRacingData = function() {
        var data = ""
        var url = ServiceUrl.GET_HORSE_TOMORROWS_RACING_DATA;
        var params = "";
        var method = "GET";
        var deferred = $q.defer();
        HttpRequester.makeRequest(method, url, params, data).then(function(res) {

            var racingData = [];
            if (res.errorCode == ErrorCode.SUCCESS) {
                angular.forEach(res.data.meetings, function(data) {
                    var obj = data;
                    obj.races = res.data.races[obj.id] ? res.data.races[obj.id] : [];
                    obj.multiPools = res.data.multiPools[obj.id] ? res.data.multiPools[obj.id] : [];

                    racingData.push(data);
                   

                });

                deferred.resolve({
                    result: true,
                    data: racingData
                });
            } else {
                deferred.resolve({
                    result: false,
                    message: res.errorMessage
                });
            }

        });
        return deferred.promise;
    }

    racingService.getTrotTomorrowsRacingData = function() {

        var data = ""
        var url = ServiceUrl.GET_TROT_TOMORROWS_RACING_DATA;
        var params = "";
        var method = "GET";
        var deferred = $q.defer();
        HttpRequester.makeRequest(method, url, params, data).then(function(res) {

            var racingData = [];
            if (res.errorCode == ErrorCode.SUCCESS) {
                angular.forEach(res.data.meetings, function(data) {
                    var obj = data;
                    obj.races = res.data.races[obj.id] ? res.data.races[obj.id] : [];
                    obj.multiPools = res.data.multiPools[obj.id] ? res.data.multiPools[obj.id] : [];

                    racingData.push(obj);
                    

                });

                deferred.resolve({
                    result: true,
                    data: racingData
                });
            } else {
                deferred.resolve({
                    result: false,
                    message: res.errorMessage
                });
            }

        });
        return deferred.promise;

    }

    racingService.getGreyhoundTomorrowsRacingData = function() {

        var data = ""
        var url = ServiceUrl.GET_GREYHOUND_TOMORROWS_RACING_DATA;
        var params = "";
        var method = "GET";
        var deferred = $q.defer();
        HttpRequester.makeRequest(method, url, params, data).then(function(res) {

            var racingData = [];
            if (res.errorCode == ErrorCode.SUCCESS) {
                angular.forEach(res.data.meetings, function(data) {
                    var obj = data;
                    obj.races = res.data.races[obj.id] ? res.data.races[obj.id] : [];
                    obj.multiPools = res.data.multiPools[obj.id] ? res.data.multiPools[obj.id] : [];

                    racingData.push(obj);
                    

                });

                deferred.resolve({
                    result: true,
                    data: racingData
                });
            } else {
                deferred.resolve({
                    result: false,
                    message: res.errorMessage
                });
            }

        });
        return deferred.promise;

    }




    // Race results services
    racingService.getHorseRaceResults = function() {
        var data = ""
        var url = ServiceUrl.GET_HORSE_RACE_RESULTS;
        var params = "";
        var method = "GET";
        var deferred = $q.defer();
        HttpRequester.makeRequest(method, url, params, data).then(function(res) {

            var racingData = [];
            if (res.errorCode == ErrorCode.SUCCESS) {
                angular.forEach(res.data.meetings, function(data) {
                    var obj = data;
                    obj.races = res.data.races[obj.id] ? res.data.races[obj.id] : [];
                    racingData.push(data);
                    

                });

                deferred.resolve({
                    result: true,
                    data: racingData
                });
            } else {
                deferred.resolve({
                    result: false,
                    message: res.errorMessage
                });
            }

        });
        return deferred.promise;
    }

    racingService.getTrotRaceResults = function() {

        var data = ""
        var url = ServiceUrl.GET_TROT_RACE_RESULTS;
        var params = "";
        var method = "GET";
        var deferred = $q.defer();
        HttpRequester.makeRequest(method, url, params, data).then(function(res) {

            var racingData = [];
            if (res.errorCode == ErrorCode.SUCCESS) {
                angular.forEach(res.data.meetings, function(data) {
                    var obj = data;
                    obj.races = res.data.races[obj.id] ? res.data.races[obj.id] : [];
                    racingData.push(obj);
                   

                });

                deferred.resolve({
                    result: true,
                    data: racingData
                });
            } else {
                deferred.resolve({
                    result: false,
                    message: res.errorMessage
                });
            }

        });
        return deferred.promise;

    }

    racingService.getGreyhoundRaceResults = function() {

        var data = ""
        var url = ServiceUrl.GET_GREYHOUND_RACE_RESULTS;
        var params = "";
        var method = "GET";
        var deferred = $q.defer();
        HttpRequester.makeRequest(method, url, params, data).then(function(res) {
            var racingData = [];
            if (res.errorCode == ErrorCode.SUCCESS) {
                angular.forEach(res.data.meetings, function(data) {
                    var obj = data;
                    obj.races = res.data.races[obj.id] ? res.data.races[obj.id] : [];
                    racingData.push(obj);
                   

                });

                deferred.resolve({
                    result: true,
                    data: racingData
                });
            } else {
                deferred.resolve({
                    result: false,
                    message: res.errorMessage
                });
            }

        });
        return deferred.promise;

    }
   
    // Race Details Service
    racingService.getRaceDetails = function(id) {

        var data = ""
        var url = ServiceUrl.GET_RACE_DETAILS+"/"+id;
        var params = "";
        var method = "GET";
        var deferred = $q.defer();
        HttpRequester.makeRequest(method, url, params, data).then(function(res) {
            var raceDetails = {};
            if (res.errorCode == ErrorCode.SUCCESS) {
               raceDetails = res.data;

                deferred.resolve({
                    result: true,
                    data: raceDetails
                });
            } else {
                deferred.resolve({
                    result: false,
                    message: res.errorMessage
                });
            }

        });
        return deferred.promise;

    }

     // Race Pools Service
    racingService.getRacePools = function(ids) {

        var data = ""
        var url = ServiceUrl.GET_RACE_POOLS+"/"+ids;
        var params = "";
        var method = "GET";
        var deferred = $q.defer();
        HttpRequester.makeRequest(method, url, params, data).then(function(res) {
            var poolsData = {};
            if (res.errorCode == ErrorCode.SUCCESS) {
               poolsData = res.data;

                deferred.resolve({
                    result: true,
                    data: poolsData
                });
            } else {
                deferred.resolve({
                    result: false,
                    message: res.errorMessage
                });
            }

        });
        return deferred.promise;

    }
//Multi-Legs racing services
    racingService.getMultiLegsRacingData = function() {
        var data = ""
        var url = ServiceUrl.GET_MULTI_LEGS_RACING_DATA;
        var params = "";
        var method = "GET";
        var deferred = $q.defer();
        HttpRequester.makeRequest(method, url, params, data).then(function(res) {

            var racingData = [];
            if (res.errorCode == ErrorCode.SUCCESS) {
                angular.forEach(res.data.poolTypes, function(data) {
                    var obj = data;
                    obj.pools = res.data.pools[obj.key] ? res.data.pools[obj.key] : [];
                    racingData.push(data);
                  

                });

                deferred.resolve({
                    result: true,
                    data: racingData
                });
            } else {
                deferred.resolve({
                    result: false,
                    message: res.errorMessage
                });
            }

        });
        return deferred.promise;
    }

    // MultiLegs Race Details Service
    racingService.getMultiLegsRaceDetails = function(id) {

        var data = ""
        var url = ServiceUrl.GET_MULTI_LEGS_RACING_DATA+"/"+id;
        var params = "";
        var method = "GET";
        var deferred = $q.defer();
        HttpRequester.makeRequest(method, url, params, data).then(function(res) {
            var raceDetails = {};
            if (res.errorCode == ErrorCode.SUCCESS) {
               raceDetails = res.data;

                deferred.resolve({
                    result: true,
                    data: raceDetails
                });
            } else {
                deferred.resolve({
                    result: false,
                    message: res.errorMessage
                });
            }

        });
        return deferred.promise;

    }

    //Futures services
    racingService.getHorseFuturesRacingData = function() {
        var data = ""
        var url = ServiceUrl.GET_HORSE_FUTURES_RACING_DATA;
        var params = "";
        var method = "GET";
        var deferred = $q.defer();
        HttpRequester.makeRequest(method, url, params, data).then(function(res) {

            var racingData = [];
            if (res.errorCode == ErrorCode.SUCCESS) {
                angular.forEach(res.data.meetings, function(data) {
                    var obj = data;
                    obj.races = res.data.races[obj.id] ? res.data.races[obj.id] : [];
                    obj.multiPools = res.data.multiPools[obj.id] ? res.data.multiPools[obj.id] : [];

                    racingData.push(data);
                  

                });

                deferred.resolve({
                    result: true,
                    data: racingData
                });
            } else {
                deferred.resolve({
                    result: false,
                    message: res.errorMessage
                });
            }

        });
        return deferred.promise;
    }

    racingService.getTrotFuturesRacingData = function() {

        var data = ""
        var url = ServiceUrl.GET_TROT_FUTURES_RACING_DATA;
        var params = "";
        var method = "GET";
        var deferred = $q.defer();
        HttpRequester.makeRequest(method, url, params, data).then(function(res) {

            var racingData = [];
            if (res.errorCode == ErrorCode.SUCCESS) {
                angular.forEach(res.data.meetings, function(data) {
                    var obj = data;
                    obj.races = res.data.races[obj.id] ? res.data.races[obj.id] : [];
                    obj.multiPools = res.data.multiPools[obj.id] ? res.data.multiPools[obj.id] : [];

                    racingData.push(obj);
                    // data.state = racingService.urlMap[entry.key];

                });

                deferred.resolve({
                    result: true,
                    data: racingData
                });
            } else {
                deferred.resolve({
                    result: false,
                    message: res.errorMessage
                });
            }

        });
        return deferred.promise;

    }

    racingService.getGreyhoundFuturesRacingData = function() {

        var data = ""
        var url = ServiceUrl.GET_GREYHOUND_FUTURES_RACING_DATA;
        var params = "";
        var method = "GET";
        var deferred = $q.defer();
        HttpRequester.makeRequest(method, url, params, data).then(function(res) {

            var racingData = [];
            if (res.errorCode == ErrorCode.SUCCESS) {
                angular.forEach(res.data.meetings, function(data) {
                    var obj = data;
                    obj.races = res.data.races[obj.id] ? res.data.races[obj.id] : [];
                    obj.multiPools = res.data.multiPools[obj.id] ? res.data.multiPools[obj.id] : [];

                    racingData.push(obj);
                });

                deferred.resolve({
                    result: true,
                    data: racingData
                });
            } else {
                deferred.resolve({
                    result: false,
                    message: res.errorMessage
                });
            }

        });
        return deferred.promise;

    }

    //Early Market services
    racingService.getHorseEarlyMarketRacingData = function() {
        var data = ""
        var url = ServiceUrl.GET_HORSE_EARLY_MARKET_RACING_DATA;
        var params = "";
        var method = "GET";
        var deferred = $q.defer();
        HttpRequester.makeRequest(method, url, params, data).then(function(res) {

            var racingData = [];
            if (res.errorCode == ErrorCode.SUCCESS) {
                angular.forEach(res.data.meetings, function(data) {
                    var obj = data;
                    obj.races = res.data.races[obj.id] ? res.data.races[obj.id] : [];
                    obj.multiPools = res.data.multiPools[obj.id] ? res.data.multiPools[obj.id] : [];

                    racingData.push(data);
                  

                });

                deferred.resolve({
                    result: true,
                    data: racingData
                });
            } else {
                deferred.resolve({
                    result: false,
                    message: res.errorMessage
                });
            }

        });
        return deferred.promise;
    }

    racingService.getTrotEarlyMarketRacingData = function() {

        var data = ""
        var url = ServiceUrl.GET_TROT_EARLY_MARKET_RACING_DATA;
        var params = "";
        var method = "GET";
        var deferred = $q.defer();
        HttpRequester.makeRequest(method, url, params, data).then(function(res) {

            var racingData = [];
            if (res.errorCode == ErrorCode.SUCCESS) {
                angular.forEach(res.data.meetings, function(data) {
                    var obj = data;
                    obj.races = res.data.races[obj.id] ? res.data.races[obj.id] : [];
                    obj.multiPools = res.data.multiPools[obj.id] ? res.data.multiPools[obj.id] : [];

                    racingData.push(obj);
                    // data.state = racingService.urlMap[entry.key];

                });

                deferred.resolve({
                    result: true,
                    data: racingData
                });
            } else {
                deferred.resolve({
                    result: false,
                    message: res.errorMessage
                });
            }

        });
        return deferred.promise;

    }

    racingService.getGreyhoundEarlyMarketRacingData = function() {

        var data = ""
        var url = ServiceUrl.GET_GREYHOUND_EARLY_MARKET_RACING_DATA;
        var params = "";
        var method = "GET";
        var deferred = $q.defer();
        HttpRequester.makeRequest(method, url, params, data).then(function(res) {

            var racingData = [];
            if (res.errorCode == ErrorCode.SUCCESS) {
                angular.forEach(res.data.meetings, function(data) {
                    var obj = data;
                    obj.races = res.data.races[obj.id] ? res.data.races[obj.id] : [];
                    obj.multiPools = res.data.multiPools[obj.id] ? res.data.multiPools[obj.id] : [];

                    racingData.push(obj);
                });

                deferred.resolve({
                    result: true,
                    data: racingData
                });
            } else {
                deferred.resolve({
                    result: false,
                    message: res.errorMessage
                });
            }

        });
        return deferred.promise;

    }

    racingService.addorEditBetSlip = function(entrantId) {

        var data = ""
        var url = ServiceUrl.ADD_OR_EDIT_BETS+entrantId;
        var params = "";
        var method = "GET";
        var deferred = $q.defer();
        HttpRequester.makeRequest(method, url, params, data).then(function(res) {

            if (res.errorCode == ErrorCode.SUCCESS) {

                deferred.resolve({
                    result: true,
                    data: res.data
                });
            } else {
                deferred.resolve({
                    result: false,
                    message: res.errorMessage
                });
            }

        });
        return deferred.promise;

    }
    
    racingService.betConfirm = function(betdata) {
       
        var url = ServiceUrl.BET_SUBMISSION_FOR_SINGLES;
        var params = "";
        var method = "POST";
        var deferred = $q.defer();
        HttpRequester.makeRequest(method, url, params, betdata).then(function(res) {

            if (res.errorCode == ErrorCode.SUCCESS) {
                deferred.resolve({
                    result: true,
                    data: res
                });
            } else {
                deferred.resolve({
                    result: false,
                    message: res.errorMessage
                });
            }

        });
        return deferred.promise;
    }

    racingService.getWagerStatus = function(wagerId) {
        var data = ""
        var url = ServiceUrl.WAGER_STATUS+wagerId;
        var params = "";
        var method = "GET";
        var deferred = $q.defer();
        HttpRequester.makeRequest(method, url, params, data).then(function(res) {

            if (res.errorCode == ErrorCode.SUCCESS) {
                deferred.resolve({
                    result: true,
                    data: res.data
                });
            } else {
                deferred.resolve({
                    result: false,
                    message: res.errorMessage
                });
            }

        });
        return deferred.promise;
    }

    racingService.getWalletStatus = function() {
        var data = ""
        var url = ServiceUrl.WALLET_STATUS;
        var params = "";
        var method = "GET";
        var deferred = $q.defer();
        HttpRequester.makeRequest(method, url, params, data).then(function(res) {

            if (res.errorCode == ErrorCode.SUCCESS) {
                deferred.resolve({
                    result: true,
                    data: res.data
                });
            } else {
                deferred.resolve({
                    result: false,
                    message: res.errorMessage
                });
            }

        });
        return deferred.promise;
    }
    

    racingService.addBetSlipToLocalStorage = function(entrant, raceDetails, pool, raceSubType) {
        var betSlipData = [];
        var betSlipDataFromLocalStorage = JSON.parse(localStorage.getItem("betSlipData"));
        if(betSlipDataFromLocalStorage){
            for(var i=0;i<betSlipDataFromLocalStorage.length;i++){
                betSlipData.unshift(betSlipDataFromLocalStorage[i])
            }
        }
        var betSlip = {venue:"", runnerName:"", rugNumber:"", entrantId:"", odds:"", oddsId:"" ,raceNumber:"", bookmaker:"", value:0.0, betType:[], betSubType:[]};
        betSlip.venue = raceDetails.race.venue;
        betSlip.raceNumber = raceDetails.race.raceNumber;
        betSlip.runnerName = entrant.runnerName;
        betSlip.entrantId = entrant.id;
        betSlip.rugNumber = entrant.rugNumber;
        betSlip.odds = (raceDetails.odds[pool.id][entrant.id].odds/100).toFixed(2);
        betSlip.oddsId = raceDetails.odds[pool.id][entrant.id].id;
        betSlip.bookmaker = pool.bookmaker;
        betSlip.betType.push($filter('displaypooltype')(pool.type));
        betSlip.betSubType.push(raceSubType);
        
        var betSlipCondition = racingService.containsObject(betSlip, betSlipData);
        if(!betSlipCondition){
            betSlipData.unshift(betSlip);
            localStorage.setItem("betSlipData",JSON.stringify(betSlipData));
            $rootScope.bets +=1; 
        }else if(betSlipCondition === 'updated'){
            alert("Updated Successfully.");
        }
        else{
            alert("This selection is already in your bet slip.");
        }
        
        
    }

    racingService.containsObject= function(obj, list) {
        var i;
        for (i = 0; i < list.length; i++) {
            if (list[i].entrantId === obj.entrantId) {
                if(list[i].odds === obj.odds){
                    return true;
                }else{
                    list[i].odds = obj.odds;
                    localStorage.setItem("betSlipData",JSON.stringify(list));
                    return 'updated';
                }
                
                
            }
        }
    
        return false;
    }

    racingService.findCombinations = function(col1, col2){
        var combinations = [];
        for(var i = 0; i < col1.length; i++)
        {
             for(var j = 0; j < col2.length; j++)
             {
                combinations.push((col1[i]+'')+ ','+(col2[j]+''));
             }
             
        }
        return combinations;
    }

    racingService.addExoticBetsToBetSlip = function(exoticArray, combinations){
            var exoticbets = { bets:[] };
              exoticbets.bets = exoticArray;
              exoticbets.bets.combinations = combinations;
              var betsData = [];
              var betsDataFromLocalStorage = JSON.parse(localStorage.getItem("exoticsBetsData"));
              if(betsDataFromLocalStorage){
                  for(var i=0;i<betsDataFromLocalStorage.length;i++){
                      betsData.unshift(betsDataFromLocalStorage[i]);
                  }
              }

              $rootScope.bets +=1;
              betsData.unshift(exoticbets);
              localStorage.setItem("ExoticbetData",JSON.stringify(betsData));
              cosole.log(1);


        
    }

    racingService.removeBetItem = function(index, data, dbName){
        for (i=0;i<data.length;i++){
            if (i == index){
                data.splice(i,1);
                $rootScope.bets-=1;
                if(data.length!=0){
                    
                    localStorage.setItem(dbName,JSON.stringify(data));
                }else{
                    localStorage.removeItem(dbName);
                    $rootScope.bets = 0;
                }
             }
        }
    }

    return racingService;
}]);
