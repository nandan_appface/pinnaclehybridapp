homeModule.controller('HomeController', ['$scope', 'HomeService', '$rootScope', '$state', function($scope, HomeService, $rootScope, $state) {


  	console.log("I am in home controller");
    
       HomeService.getHomeImages().then(function(res) {
            if (res.result) {
                $scope.homeMenu = res.data;
            }

        });

       HomeService.nextToJumpRaces().then(function(res) {
            if (res.result) {
                $scope.nextToJumpRaces = res.data;
            }

        });
    
}]);
