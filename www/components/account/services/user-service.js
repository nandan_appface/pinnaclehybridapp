accountModule.factory('UserService', ['ServiceUrl', 'HttpRequester', 'ErrorCode', '$q', function(ServiceUrl, HttpRequester, ErrorCode, $q, $rootScope) {
    var userService = {};

    userService.getProfile = function() {
        //Preparing data
        var data = ""
        var url = ServiceUrl.GET_USER_PROFILE;
        var params = "";
        var method = "GET";
        var deferred = $q.defer();

        //Making Http Request
        HttpRequester.makeRequest(method, url, params, data, true).then(function(res) {

            if (res.errorCode == ErrorCode.SUCCESS) {
                
                deferred.resolve({
                    data: res.data
                });
            } 
            else if(res.errorCode == ErrorCode.UNAUTHENTICATED){
                  deferred.resolve({
                    errorCode : ErrorCode.UNAUTHENTICATED,
                    message: res.errorMessage
                });
            }
            else {
                deferred.resolve({
                    message: res.errorMessage
                });
            }

        });
        return deferred.promise;
    }

    userService.updateProfile = function(userInfo) {
        var usrData = {
            "emailAddress": userInfo.emailAddress ? userInfo.emailAddress : "",
            "mobileNumber": userInfo.mobileNumber ? userInfo.mobileNumber : "",
            "countryCode": userInfo.countryCode ? userInfo.countryCode : "",
            "areaCode": userInfo.areaCode ? userInfo.areaCode : "",
            "phoneNumber": userInfo.phoneNumber ? userInfo.phoneNumber : "",
            "addressFlatNumber": userInfo.addressFlatNumber ? userInfo.addressFlatNumber : "",
            "addressStreetNumber": userInfo.addressStreetNumber ? userInfo.addressStreetNumber : "",
            "addressStreet": userInfo.addressStreet ? userInfo.addressStreet : "",
            "addressStreetType": userInfo.addressStreetType ? userInfo.addressStreetType : "",
            "addressSuburb": userInfo.addressSuburb ? userInfo.addressSuburb : "",
            "addressPostcode": userInfo.addressPostcode ? userInfo.addressPostcode : "",
            "addressState": userInfo.addressState ? userInfo.addressState : "",
            "addressCountry": userInfo.country.name ?  userInfo.country.name  : "",
            "addressLine1": userInfo.addressLine1 ? userInfo.addressLine1 : "",
            "addressLine2": userInfo.addressLine2 ? userInfo.addressLine2 : "", 
            "addressLine3": userInfo.addressLine3 ? userInfo.addressLine3 : ""
        };



        var url = ServiceUrl.UPDATE_USER_PROFILE;
        var params = "";
        var method = "PUT";
        var deferred = $q.defer();
        HttpRequester.makeRequest(method, url, params, usrData).then(function(res) {

            if (res.errorCode == ErrorCode.SUCCESS) {
               
                deferred.resolve({
                    result: true,
                    data: res.data
                });
            } else {
                deferred.resolve({
                    result: false,
                    message: res.errorMessage
                });
            }

        });
        return deferred.promise;
    }

    return userService;
}]);
