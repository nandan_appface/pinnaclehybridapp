racingModule.controller('TodaysRaceController', ['$scope', '$stateParams', 'RacingService', function($scope, $stateParams, RacingService) {

    // $scope.rows = [
    // 	"BAIRNSDALE",
    // 	"COFFS HARBOUR",
    // 	"PORTLAND MEADOWS",
    // 	"QUEANBEYAN",
    // 	"SANTIAGO",
    // ]

    $scope.oneAtATime = true;
     $scope.status = {
        open : false
     };


    $scope.init = function() {

        $scope.racingData = "";


        switch ($stateParams.source) {

            case "R":
            	$scope.pageTitle = "HORSE RACING";
            	$scope.icon = "images/orangeicons/horse.png";
                RacingService.getHorseTodaysRacingData().then(function(res) {
                    if (res.result) {

                        $scope.racingData = res.data;

                    }

                });
                break;
            case "T":
            $scope.pageTitle = "HARNESS RACING";
            	$scope.icon = "images/orangeicons/trot.png";
                RacingService.getTrotTodaysRacingData().then(function(res) {
                    if (res.result) {

                        $scope.racingData = res.data;

                    }

                });
                break;

            case "G":
            	$scope.pageTitle = "GREYHOUND RACING";
            	$scope.icon = "images/orangeicons/greyhound.png";
                RacingService.getGreyhoundTodaysRacingData().then(function(res) {
                    if (res.result) {

                        $scope.racingData = res.data;

                    }

                });
                break;



        }



    

}
    

}]);
