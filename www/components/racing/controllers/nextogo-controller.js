
racingModule.controller('NextogoController', ['$scope', '$stateParams','RacingService', function($scope, $stateParams,RacingService){




       $scope.init = function() {

        $scope.racingData = "";


        switch ($stateParams.source) {

            case "R":
             $scope.pageTitle = "HORSE RACING";
             $scope.icon = "images/orangeicons/horse.png";
            
                RacingService.getHorseNextogoRacingData().then(function(res) {

                   
                    if (res.result) {

                        $scope.racingData = res.data;

                    }

                });
                break;
            case "T":
            $scope.pageTitle = "HARNESS RACING";
            	$scope.icon = "images/orangeicons/trot.png";
                RacingService.getTrotNextogoRacingData().then(function(res) {
                    if (res.result) {

                        $scope.racingData = res.data;

                    }

                });
                break;

            case "G":
            	$scope.pageTitle = "GREYHOUND RACING";
            	$scope.icon = "images/orangeicons/greyhound.png";
                RacingService.getGreyhoundNextogoRacingData().then(function(res) {
                    if (res.result) {

                        $scope.racingData = res.data;

                    }

                });
                break;



        }



    }
	
}]);
