racingModule.controller('RacingController', ['$scope', 'RacingService', '$state', 'RouteHistoryService', function($scope, RacingService, $state, RouteHistoryService) {

    $scope.init = function() {
        $scope.menuData = {};
        RacingService.getHorseRacingMenu().then(function(res) {
            if (res.result) {
                $scope.menuData.horse = res.data;
            }

        });
        RacingService.getTrotRacingMenu().then(function(res) {
            if (res.result) {
                $scope.menuData.trot = res.data;
            }

        });
        RacingService.getGreyhoundRacingMenu().then(function(res) {
            if (res.result) {
                $scope.menuData.greyhound = res.data;
            }

        });



    }

    $scope.goToDetailView = function(state, src) {
        $state.go(state, {source: src});
    }

    $scope.goBack = function() {

        RouteHistoryService.goBack();
    }


}]);


