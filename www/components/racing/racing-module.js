var racingModule = angular.module('RacingModule', [])

.config(function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('app.main.races', {
            url: '/races',
            views: {
                'body-content': {
                    templateUrl: 'components/racing/templates/racing-home.html',
                    controller: 'RacingController'
                }
            }


        })


    .state('app.main.races.nextogo', {
        url: '/nextogo/:source',
        views: {
            'race-body-content': {
                templateUrl: 'components/racing/templates/nextogo.html',
                controller: 'NextogoController'
            }
        },
        // params: {source : null }



    })

    .state('app.main.races.today', {
        url: '/today/:source',
        views: {
            'race-body-content': {
                templateUrl: 'components/racing/templates/todays-race.html',
                controller: 'TodaysRaceController'
            }
        },
        // params: {source : null }



    })

    .state('app.main.races.tomorrow', {
            url: '/tomorrow/:source',
            views: {
                'race-body-content': {
                    templateUrl: 'components/racing/templates/tomorrows-race.html',
                    controller: 'TomorrowsRaceController'
                }
            },
            //  params: {source : null }



        })
        .state('app.main.races.detail', {
            url: '/detail/:id',
            views: {
                'race-body-content': {
                    templateUrl: 'components/racing/templates/race-detail-page.html',
                    controller: 'RaceDetailPageController'
                }
            },
            // params: {source : null }



        })

    .state('app.main.races.result', {
        url: '/result/:source',
        views: {
            'race-body-content': {
                templateUrl: 'components/racing/templates/race-result.html',
                controller: 'RaceResultController'
            }
        },
        // params: {source : null }



    })

    .state('app.main.races.multi-legs', {
        url: '/multi-legs/:source',
        views: {
            'race-body-content': {
                templateUrl: 'components/racing/templates/multi-legs.html',
                controller: 'MultiLegsController'
            }
        },
        // params: {source : null }



    })

    .state('app.main.races.multi-legs-dtails', {
        url: '/multi-legs-details/:id',
        views: {
            'race-body-content': {
                templateUrl: 'components/racing/templates/multi-legs-details.html',
                controller: 'MultiLegsDetailsController'
            }
        },
        // params: {source : null }



    })
    .state('app.main.races.early-market', {
        url: '/early-market/:source',
        views: {
            'race-body-content': {
                templateUrl: 'components/racing/templates/early-market.html',
                controller: 'EarlyMarketController'
            }
        },
        // params: {source : null }



    })
    .state('app.main.races.futures', {
        url: '/futures/:source',
        views: {
            'race-body-content': {
                templateUrl: 'components/racing/templates/futures.html',
                controller: 'FuturesController'
            }
        },
        // params: {source : null }



    })
    .state('app.main.betslip', {
        url: '/betslip',
        views: {
            'body-content': {
                templateUrl: 'components/racing/templates/bet-slip.html',
                controller: 'BetSlipController'
            }
        },
    })  

    .state('app.betslipconfirm', {
            url: '/betslipconfirm',
            templateUrl: 'components/racing/templates/bet-slip-confirm.html',
            controller: 'BetSlipConfirmController'
    })

    .state('app.betsuccess', {
            url: '/betsuccess',
            templateUrl: 'components/racing/templates/bet-success.html',
            controller: 'BetSuccessController',
            params: {wagers: null}
    })

    .state('app.singlebet', {
            url: '/singlebet',
            templateUrl: 'components/racing/templates/single-bet.html',
            controller: 'SingleBetController',
            params: {singlebet: null}
    })

});

// Old code
racingModule.filter('displaystatus_old', function($filter) {

   // Create the return function
   // set the required parameter name to **number**
   return function(obj) {
       var currentDate = new Date();
       var currentTime = currentDate.getTime();
       var timeDiff = (obj.startTime - currentTime);
       var minutes = Math.round(timeDiff / (1000 * 60));
       var displayString = "";

       if (obj.status == "OPEN") {

           if (minutes >= 60) {
               displayString = $filter('date')(obj.startTime, 'hh:mm a');
           } else if (minutes < 60 && minutes >= 0) {
               displayString = minutes + " min";
           } else if (minutes < 0) {
               displayString = minutes + " min";
           }

       } else {

           if (!obj.results || (obj.results.length == 0)) {
               displayString = obj.status;
           } else {
               displayString = obj.results.join(", ");
           }

       }

       return displayString;

   }
});

racingModule.filter('displaystatus', function($filter) {

    // Create the return function
    // set the required parameter name to **number**
    return function(obj) {
        if(obj){
            var currentDate = new Date();
            var currentTime = currentDate.getTime();

            var timeDiff = (obj.startTime - currentTime);
            var seconds = Math.floor(timeDiff / 1000);
            var remainderSecond = seconds % 60;
            var minutes = Math.floor(seconds / 60);
            var remainderMinute = minutes % 60;
            var hours = Math.floor(minutes / 60);
            var remainderHours = hours % 24;
            var days = Math.floor(hours / 24);
            var displayString = "";

           if (obj.status == "OPEN") {



              if (seconds<60) {
                    displayString = seconds + " s ";
               } 
               else if (minutes < 11) {
                    displayString = minutes + " m " + remainderSecond + " s " ;
               }
               else if (minutes < 60) {
                    displayString = minutes + " m ";
                }
               else if (hours<24) {
                    displayString = hours + " h " + remainderMinute + " m ";
               } 
                else if (days > 0) {
                   displayString = days + " d " + remainderHours + " h ";

               }

            } else {

                if (!obj.results || (obj.results.length == 0)) {
                    displayString = obj.status;
                } else {
                    displayString = obj.results.join(", ");
                }

            }
        }
        return displayString;

    }
});


racingModule.filter('displaypooltype', function($filter) {

    // Create the return function
    // set the required parameter name to **number**
    return function(type) {
        var displayString = "";


        switch (type) {
            case 'W':
                displayString = "Win"; //Win
                break;
            case 'P':
                displayString = "Place";
                break;
            case 'BT':
                displayString = "BT"; //Best Tote
                break;
            case 'TF':
                displayString = "TF";
                break;
            case 'BT3SP':
                displayString = "BT3SP"; //Best of 3 Totes + Starting Price
                break;
            case 'BT3W':
                displayString = "BT3W";
                break;
            case 'Q':
                displayString = "Quinella";
                break;
            case 'E':
                displayString = "Exacta";
                break;
            case 'T':
                displayString = "Trifecta";
                break;

            case 'DD':
                displayString = "Daily Double";
                break;
            case 'QD':
                displayString = "Quadrella";
                break;
            case 'DU':
                displayString = "Duet";
                break;
            case 'FF':
                displayString = "First Four";
                break;
              case 'XD':
                displayString = "Extra Double";
                break;
              case 'RD':
                displayString = "Running Double";
                break;
             case 'TT':
                displayString = "Treble";
                break;
             case 'BG6':
                displayString = "Big 6";
                break;
            default:
                displayString = type;
                break;

        }

        return displayString;


    }
});


racingModule.filter('displaywinnerposition', function($filter) {

    // Create the return function
    // set the required parameter name to **number**
    return function(pos) {
        var displayString = "";


        switch (pos) {
            case 1:
                displayString = "1st Place";
                break;
            case 2:
                displayString = "2nd Place";
                break;
            case 3:
                displayString = "3rd Place";
                break;
            case 4:
                displayString = "4th Place";
                break;


        }

        return displayString;


    }
});


racingModule.filter('bookmarkerdisplay', function($filter) {

    // Create the return function
    // set the required parameter name to **number**
    return function(bookMarkerId) {
        var displayString = "";


        switch (bookMarkerId) {
            case 'N':
                displayString = "NSW/PL";
                break;
            case 'U':
                displayString = "UNI/PL";
                break;
            case 'V':
                displayString = "VIC/PL";
                break;
            case 'TobRacing':
                displayString = "STAB/PL";
                break;


        }

        return displayString;


    }
});
racingModule.filter('twofixed', function($filter) {

    // Create the return function
    // set the required parameter name to **number**
    return function(value) {
        var displayString = 0.00;
        displayString = value.toFixed(2);


        return displayString;


    }
});