racingModule.controller('BetSuccessController', ['$scope', '$stateParams','RouteHistoryService','RacingService','$rootScope', function($scope, $stateParams, RouteHistoryService, RacingService, $rootScope) {

	var wagersData = $stateParams.wagers;
	$scope.totalbetsSucces =0;

    var bets= JSON.parse(localStorage.getItem("betSlipData"));
    
    for(var i=0;i<bets.length;i++){
        for(var j=0;j<wagersData.length;j++){
            if(bets[i].referenceId==wagersData[j].clientReferenceId){
                bets[i].wagerId = wagersData[j].wagerId;
            }
        }
    }
    
    $scope.betsSuccessData = bets;
    submitBets(wagersData);

    function submitBets(wagersData){
        var pendingBets = [];
            for(var i=0; i<wagersData.length;i++){
                $scope.wagerindex = i;

                RacingService.getWagerStatus(wagersData[i].wagerId).then(function(res) {
                    if (res.result) {
                       

                        var wager = res.data.wager;
                        if(wager.status.key=='S' || wager.status.key=='F'|| wager.status.key=='E'){
                            for(var j=0;j<$scope.betsSuccessData.length;j++){
                                if($scope.betsSuccessData[j].wagerId==wager.id){
                                    
                                    RacingService.getWalletStatus().then(function(result){
                                         $rootScope.loggedInUserDetail.wallet = result.data.walletDto;
                                     });

                                    $scope.betsSuccessData[j].wager = wager;
                                    $scope.totalbetsSucces +=1;
                                    wagersData.splice($scope.wagerindex,1);
                                }
                            }
                        }
                        else if(wager.status.key=='N' || wager.status.key=='SIP'){
                            console.log(wagersData);
                            if((wagersData.length-1) === $scope.wagerindex){
                                submitBets(wagersData);
                            }
                        }else{
                            console.log("Error")
                        }
                         
                    }

                });
                
            }
        localStorage.removeItem("betSlipData");
        $rootScope.bets = 0;
        
    }
    

}]);