racingModule.controller('FuturesController', ['$scope', '$stateParams', 'RacingService', function($scope, $stateParams, RacingService) {

    // $scope.rows = [
    // 	"BAIRNSDALE",
    // 	"COFFS HARBOUR",
    // 	"PORTLAND MEADOWS",
    // 	"QUEANBEYAN",
    // 	"SANTIAGO",
    // ]

    $scope.oneAtATime = true;
     $scope.status = {
        open : false
     };


    $scope.init = function() {

        $scope.racingData = "";


        switch ($stateParams.source) {

            case "R":
            	$scope.pageTitle = "HORSE RACING";
            	$scope.icon = "images/orangeicons/horse.png";
                RacingService.getHorseFuturesRacingData().then(function(res) {
                    if (res.result) {

                        $scope.racingData = res.data;

                    }

                });
                break;
            case "T":
            $scope.pageTitle = "HARNESS RACING";
            	$scope.icon = "images/orangeicons/trot.png";
                RacingService.getTrotFuturesRacingData().then(function(res) {
                    if (res.result) {

                        $scope.racingData = res.data;


                    }

                });
                break;

            case "G":
            	$scope.pageTitle = "GREYHOUND RACING";
            	$scope.icon = "images/orangeicons/greyhound.png";
                RacingService.getGreyhoundFuturesRacingData().then(function(res) {
                    if (res.result) {

                        $scope.racingData = res.data;

                    }

                });
                break;



        }



    

}
    

}]);
