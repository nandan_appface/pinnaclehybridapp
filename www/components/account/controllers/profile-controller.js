accountModule.controller('ProfileController', ['$scope', '$stateParams', 'RouteHistoryService', 'UserService', 'CommonService', '$filter', function($scope, $stateParams, RouteHistoryService, UserService, CommonService, $filter) {

    $scope.init = function() {
        $scope.editMode = false;
        $scope.inputClass = "text-field-one-readonly";
        CommonService.getCountryList().then(function(result) {
            $scope.countries = result.countryList;


            UserService.getProfile().then(function(res) {
                if (res.data) {
                    $scope.profileInfo = res.data.accountHolderDto.details;
                    var selectedCountry = $filter('filter')($scope.countries, {
                        name: $scope.profileInfo.addressCountry
                    }, true);
                    if (selectedCountry) {
                        $scope.profileInfo.country = selectedCountry[0];
                    }

                } else {

                    alert(res.message);
                }
            });


        });



    }

    $scope.goBack = function() {
        RouteHistoryService.goBack();
    }

    $scope.changeMode = function() {

        if ($scope.editMode) {
            $scope.editMode = false;
            $scope.inputClass = "text-field-one-readonly";
        } else {
            $scope.editMode = true;
            $scope.inputClass = "text-field-one";
        }
    }



    $scope.updateProfile = function() {
        var msg = "";
        var validate = true;

        if (!$scope.profileInfo.emailAddress) {
            msg += "Please enter Email";
            validate = false;
        }


        if (!$scope.profileInfo.mobileNumber) {
            msg += "\nPlease enter Mobile Number";

            validate = false;

        } 

       
        if (!$scope.profileInfo.addressStreetNumber) {
            msg += "\nPlease enter Street / House Number";

            validate = false;

        } 


        if (!$scope.profileInfo.addressSuburb) {
            msg += "\nPlease enter Suburb";

            validate = false;

        } 


        if (!$scope.profileInfo.addressPostcode) {
            msg += "\nPlease enter Post Code";

            validate = false;

        } 
          if (!$scope.profileInfo.addressState) {
            msg += "\nPlease enter State";

            validate = false;

        } 



        if (!$scope.profileInfo.country) {
            msg += "\nPlease select Contry";

            validate = false;

        } 
        
        




        if (validate) {
            UserService.updateProfile($scope.profileInfo).then(function(res) {
                if (res.result) {
                    $scope.changeMode();
                } else {
                    alert(res.message);
                }

            });
        } else {

            alert(msg);
        }





    }

}]);
