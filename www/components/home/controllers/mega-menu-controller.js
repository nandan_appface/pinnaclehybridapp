homeModule.controller('MegaMenuController', function ($scope, $state, $uibModalInstance, items, AuthenticationService) {

  $scope.items = items;
  $scope.selected = {
    item: $scope.items[0]
  };

  $scope.ok = function () {
    $uibModalInstance.close($scope.selected.item);
  };

    $scope.logout = function() {
        AuthenticationService.logout().then(function(res){
          if(res.result)
          {
            $scope.ok();
            $state.go('app.main.home');
          }
          else {
            var msg = "";
            if(res.message){
              msg = res.message;
            } else{
              msg = "Logout failed";
            }
            alert(msg);
          }

        });
    }



  $scope.cancel = function () {
    $uibModalInstance.dismiss('cancel');
  };
});