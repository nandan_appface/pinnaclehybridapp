angular.module('Mobilewebclient', [
    'ui.router',
    'mobile-angular-ui',
    'CommonModule',
    'HomeModule',
    'HttpRequesterModule',
    'AccountModule',
    'UtilModule',
    'RacingModule',
    'ui.bootstrap',
    'angularSpinner',
    'mobile-angular-ui.core.activeLinks',
    'angular.filter',
    'angular-carousel'
])
.run(function(RouteHistoryService, UserService, ErrorCode, $rootScope) { 
  
    //Checking authentication
    UserService.getProfile().then(function(res){
        if(res.data){
             $rootScope.isLoggedin = true;
             $rootScope.loggedInUserDetail = res.data.accountHolderDto;
        }
        else if(res.errorCode == ErrorCode.UNAUTHENTICATED){
             $rootScope.isLoggedin = false;
        }
        else {

             $rootScope.isLoggedin = false;
        }
    });

})

.config(function($stateProvider, $httpProvider, $urlRouterProvider) {


      $httpProvider.defaults.withCredentials = true;
      $httpProvider.defaults.useXDomain = true;
      $httpProvider.defaults.headers.common['X-Requested-With'];

    $stateProvider

    // setup an abstract state for app
    .state('app', {
            url: '',
            abstract: true,
            templateUrl: 'components/common/templates/blank.html'
        })
        .state('app.main', {
            url: '/main',
            templateUrl: 'components/common/templates/main.html',
            controller: 'MainController'
        })

    .state('app.main.home', {
        url: '/home',
        views: {
            'body-content': {
                templateUrl: 'components/home/templates/home.html',
                controller: 'HomeController'
            }
        }


    });

    $urlRouterProvider.otherwise('/main/home');





    // Each tab has its own nav history stack:



    // $stateProvider.state("contacts", {
    //   template: '<h1>{{title}}</h1>',
    //   resolve: { title: 'My Contacts' },
    //   controller: function($scope, title){
    //     $scope.title = 'My Contacts';
    //   },
    //   onEnter: function(title){
    //     if(title){ ... do something ... }
    //   },
    //   onExit: function(title){
    //     if(title){ ... do something ... }
    //   }
    // });

});
