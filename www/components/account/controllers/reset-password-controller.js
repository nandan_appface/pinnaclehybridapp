accountModule.controller('ResetPasswordController', ['$scope', 'AuthenticationService', '$location', '$state', function($scope, AuthenticationService, $location, $state) {

    $scope.resetPassword = function() {

        var msg = "";
        var validate = true;
        var url = $location.url();
        var params = $location.search();

        $scope.info.username = params.d;
        $scope.info.token = params.t;




        if (!$scope.info.password) {
            msg += "Please enter password";
            validate = false;
        }else{
            if($scope.info.password.length<6)
            {
                msg += "Password should be atleast 6 characters long";
                validate = false;
            }
        }


        if (!$scope.info.confirmPassword) {
            msg += "\nPlease confirm password";

            validate = false;

        }

        if ($scope.info.password && $scope.info.confirmPassword) {

            if ($scope.info.password != $scope.info.confirmPassword) {
                msg = "Password and Confirm Password did not match";

                validate = false
            }

        }

        if (validate) {

            AuthenticationService.resetPassword($scope.info).then(function(result) {
                if (result.result) {

                    $state.go('app.main.home');

                } else {

                    alert(result.message);

                }
            });
        } else {

            alert(msg);

        }

    }


}]);
