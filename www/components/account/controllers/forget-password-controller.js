accountModule.controller('ForgetPasswordController', ['$scope', 'AuthenticationService', function($scope, AuthenticationService) {

    $scope.sendEmail = function() {
        var msg = "";
        var validate = true;

        if (!$scope.info.username) {
            msg += "Please enter Username";
            validate = false;
        }

        if (!$scope.info.email) {
            msg += "\nPlease enter Email";

            validate = false;

        }
        if (validate) {

            AuthenticationService.sendResetPasswordEmail($scope.info.username, $scope.info.email).then(function(result) {
                if (result.result) {

                    alert("An email with  password reset link has been sent to your email.");

                } else {

                    alert(result.message);

                }
            });
        } else {
            alert(msg);

        }

    }


}]);
