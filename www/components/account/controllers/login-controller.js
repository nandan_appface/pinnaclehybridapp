accountModule.controller('LoginController', ['$scope', 'AuthenticationService', '$rootScope', '$state','RouteHistoryService', function($scope, AuthenticationService, $rootScope, $state, RouteHistoryService) {


    $scope.doLogin = function() {

        AuthenticationService.login($scope.user.username, $scope.user.password).then(function(result) {
            if (result.result) {

                //$rootScope.Ui.turnOn('passwordModal');
                // $state.go('app.main.home');
                RouteHistoryService.goBack();

                if ($scope.user.canRemember)
                    AuthenticationService.rememberMe($scope.user.username, $scope.user.password);
            } else {
                var msg = ""
                if (result.message) {
                    msg = result.message;
                } else {
                    msg = "Login failed";
                }

                alert(msg);

            }
        });
    }


    
}]);
