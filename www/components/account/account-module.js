var accountModule = angular.module('AccountModule', [])

.config(function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('app.main.login', {
            url: '/login',
            views: {
                'body-content': {
                    templateUrl: 'components/account/templates/login.html',
                    controller: 'LoginController'
                }
            }


        })
        .state('app.main.forgetPassword', {
            url: '/forgetpassowrd',
            views: {
                'body-content': {
                    templateUrl: 'components/account/templates/forget-password.html',
                    controller: 'ForgetPasswordController'
                }
            }


        })
         .state('app.main.profile', {
            url: '/profile',
            views: {
                'body-content': {
                    templateUrl: 'components/account/templates/profile.html',
                    controller: 'ProfileController'
                }
            },
            params: { userData: null }


        })
        .state('app.registration', {
            url: '/registration',

            templateUrl: 'components/account/templates/register.html',
            controller: 'RegistrationController'




        })
        .state('app.registration.step1', {
            url: '/step1',
            views: {
                'body-content': {
                    templateUrl: 'components/account/templates/reg-step1.html',

                }
            }


        })
        .state('app.registration.step2', {
            url: '/step2',
            views: {
                'body-content': {
                    templateUrl: 'components/account/templates/reg-step2.html',

                }
            }


        })
        .state('app.registration.step3', {
            url: '/step3',
            views: {
                'body-content': {
                    templateUrl: 'components/account/templates/reg-step3.html',

                }
            }


        })
        .state('app.registration.step4', {
            url: '/step4',
            views: {
                'body-content': {
                    templateUrl: 'components/account/templates/reg-step4.html',

                }
            }


        })
        .state('app.registration.step5', {
            url: '/step5',
            views: {
                'body-content': {
                    templateUrl: 'components/account/templates/reg-step5.html',

                }
            }


        })

        .state('app.resetpassword', {
            url: '/reset-password',
            
                    templateUrl: 'components/account/templates/reset-password.html',
                    controller: 'ResetPasswordController'

           


        });


});