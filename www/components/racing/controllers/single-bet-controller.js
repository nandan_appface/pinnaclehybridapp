racingModule.controller('SingleBetController', ['$scope', '$stateParams','RouteHistoryService', function($scope, $stateParams, RouteHistoryService) {

	$scope.singlebet = $stateParams.singlebet;

	$scope.goBack = function() {
		
        RouteHistoryService.goBack();
    };

}]);