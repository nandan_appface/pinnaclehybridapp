racingModule.controller('BetSlipController', ['$scope', '$stateParams','RouteHistoryService','RacingService','$uibModal','$rootScope','$state', function($scope, $stateParams, RouteHistoryService, RacingService, $uibModal,$rootScope, $state) {

	$scope.betSlipDataFromLocalStorage = JSON.parse(localStorage.getItem("betSlipData"));
    $scope.exoticsBetsData = JSON.parse(localStorage.getItem("ExoticbetData"));
	$scope.betTotalValue = 0.0;
	$scope.betEstimateTotalValue=0.0;

    if(!$rootScope.bets && $scope.betSlipDataFromLocalStorage){
        $rootScope.bets = $scope.betSlipDataFromLocalStorage.length;
    }
	

	$scope.removeBetSlipItem  = function(index, exoticname){

		// var betSlipDataFromLocalStorage = $scope.betSlipDataFromLocalStorage;
        if(!exoticname){
            RacingService.removeBetItem(index, $scope.betSlipDataFromLocalStorage, 'betSlipData');
        }else{
            RacingService.removeBetItem(index, $scope.exoticsBetsData, 'ExoticbetData');
        }
		
	};
	$scope.betMinusValue  = function(data){
		if(data.value>0){
			data.value -=5;
			data.betitemtotal = (data.value * data.odds).toFixed(2);
			$scope.getBetTotal($scope.betSlipDataFromLocalStorage);
		}
	
	};
	$scope.betPlusValue  = function(data){
		if(data.value>=0){
			data.value +=5;
			data.betitemtotal = (data.value * data.odds).toFixed(2);
			$scope.getBetTotal($scope.betSlipDataFromLocalStorage);
		}
		
	};
	$scope.getBetTotal = function(data){
		var betValue=0;
		var betEstimateValue=0;
		for(i in data){
			betValue+=data[i].value;
			betEstimateValue+= parseFloat(data[i].betitemtotal);
			$scope.betTotalValue = betValue;
			$scope.betEstimateTotalValue = betEstimateValue;
			localStorage.setItem("betSlipData",JSON.stringify(data));
		}

	};

	$scope.goBack = function() {
        RouteHistoryService.goBack();
    };

    $scope.initialiseType = function(data){
    	RacingService.addorEditBetSlip(data.entrantId).then(function(res) {
            if (res.result) {
                var result = Object.keys(res.data.singlesMenu);
                for(var i=0; i<result.length;i++){
                    if(data.betType[0] == result[i]){
                        data.type = data.betType[0];
                        break;
                    }
                    
                }
                
                data.betType = result;
            }else{
                $scope.type = data.betType[0];
            }

        });
        
    };

    $scope.changeBetType = function(data){
       RacingService.addorEditBetSlip(data.entrantId).then(function(res) {
            if (res.result) {
                var result = res.data.singlesMenu;
                data.betSubType = Object.keys(result[data.type]);
                data.subtype = data.betSubType[data.betSubType.length-1];

                if(res.data.odds[result[data.type][data.subtype][0].id].odds){
                     data.odds = (res.data.odds[result[data.type][data.subtype][0].id].odds/100).toFixed(2);
                 }else{

                 }
               
                
                 
            }else{
                data.subtype = data.betSubType[0];
            }

        });
    }

    $scope.changeBetSubType = function(data){
        data.subtype = $scope.subtype;
    }
    $scope.getType = function(data){
         RacingService.addorEditBetSlip(data.entrantId).then(function(res) {
            if (res.result) {
               return res.data;
        
            }

        });
    }
    // $scope.selectWinPlaceData = function(data){
    // 	var key = $scope.selectedFirstitem;
    // 	$scope.betWinPlaceData =  $scope.betSlipRunnerData.key;

    // }

    // $scope.betSlipRunnerSelected = function(key) {
    //     $scope.keySelected = key;
    //     $scope.SelecetdData = $scope.betSlipRunnerData[key];
    // };
    // $scope.singleBetSlipSelected = function(key){
    //   $scope.betSlipPlace = $scope.keySelected+"/"+key;
    //   $scope.testData.betSlipPlace = $scope.keySelected+"/"+key;
    // };
    $scope.close=function(){
    	$scope.SelecetdData = '';
        $scope.modalInstance.dismiss();//$scope.modalInstance.close() also works I think
        $scope.betTotalValue = 0.0;
        $scope.betEstimateTotalValue=0.0;
    };

    $scope.removeBets  = function(index){
        $rootScope.bets=0;
		localStorage.removeItem("betSlipData");
		$scope.betSlipDataFromLocalStorage = JSON.parse(localStorage.getItem("betSlipData"));
		        
	};

    $scope.bet = function(){
    	if($rootScope.isLoggedin){
    		if($scope.betSlipDataFromLocalStorage){
    			for(var i=0;i<$scope.betSlipDataFromLocalStorage.length;i++){
    				if($scope.betSlipDataFromLocalStorage[i].value > 0){
                        localStorage.setItem("betSlipData",JSON.stringify($scope.betSlipDataFromLocalStorage));
    					$state.go('app.betslipconfirm');
    				}else{
    					alert("You must enter a stake to place a bet");
    				}
    			}
    			
    		}else{
    			alert("You have no selection in your betslip");
    		}
    		
    	}else{
    		$state.go('app.main.login');

    	}
    	 
    }
}]);

// Model-open code
// $scope.modalInstance=$uibModal.open({
//     templateUrl: 'components/racing/templates/bet-slip-pop-up.html',
//     scope:$scope,
//     backdrop: 'static'
// });