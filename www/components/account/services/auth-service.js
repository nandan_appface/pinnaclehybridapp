accountModule.factory('AuthenticationService', ['ServiceUrl', 'HttpRequester', 'ErrorCode', '$q', '$rootScope', function(ServiceUrl, HttpRequester, ErrorCode, $q, $rootScope) {
    var authService = {};

    authService.login = function(username, password) {
        var usrCredentials = {
            "username": username,
            "password": password
        };
        var url = ServiceUrl.SIGN_IN;
        var params = "";
        var method = "POST";
        var deferred = $q.defer();
        HttpRequester.makeRequest(method, url, params, usrCredentials).then(function(res) {

            if (res.errorCode == ErrorCode.SUCCESS) {
                $rootScope.isLoggedin = true;
                $rootScope.loggedInUserDetail = res.data.accountHolderDto;
                deferred.resolve({
                    result: true,
                    data: res
                });
            } else {
                deferred.resolve({
                    result: false,
                    message: res.errorMessage
                });
            }

        });
        return deferred.promise;
    }
    authService.logout = function() {

        var url = ServiceUrl.SIGN_OUT;
        var params = "";
        var data = "";
        var method = "DELETE";
        var deferred = $q.defer();
        HttpRequester.makeRequest(method, url, params, data).then(function(res) {

            if (res.errorCode == ErrorCode.SUCCESS) {
                $rootScope.isLoggedin = false;
                $rootScope.loggedInUserDetail=undefined;
                deferred.resolve({
                    result: true,
                    data: res
                });
            } else {
                deferred.resolve({
                    result: false,
                    message: res.errorMessage
                });
            }

        });
        return deferred.promise;

    }
    authService.rememberMe = function(username, password) {
        console.log("Remember me get called");
    }

    authService.sendResetPasswordEmail = function(username, email) {
        var data = {
            "username": username,
            "email": email
        };
        var url = ServiceUrl.FORGET_PASSWORD;
        var params = "";
        var method = "POST";
        var deferred = $q.defer();
        HttpRequester.makeRequest(method, url, params, data).then(function(res) {

            if (res.errorCode == ErrorCode.SUCCESS) {
                deferred.resolve({
                    result: true,
                    data: res
                });
            } else {
                deferred.resolve({
                    result: false,
                    message: res.errorMessage
                });
            }

        });
        return deferred.promise;
    }

    authService.resetPassword = function(info) {

        // given url http://example.com/#/some/path?foo=bar&baz=xoxo
       // var searchObject = $location.search();
            // => {foo: 'bar', baz: 'xoxo'}

        var data = {
            
            "username": info.username,
            "password": info.password,
            "token": info.token
        };


        var url = ServiceUrl.RESET_PASSWORD;
        var params = "";
        var method = "POST";
        var deferred = $q.defer();
        HttpRequester.makeRequest(method, url, params, data).then(function(res) {

            if (res.errorCode == ErrorCode.SUCCESS) {
                deferred.resolve({
                    result: true,
                    data: res
                });
            } else {
                deferred.resolve({
                    result: false,
                    message: res.errorMessage
                });
            }

        });
        return deferred.promise;
    }


    return authService;
}]);
